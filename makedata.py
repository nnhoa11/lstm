import os
import matplotlib.pyplot as plt
import matplotlib.image as mpmig
import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import pandas as pd
import random
from keras.preprocessing import image
import keras
from keras import layers
import cv2
import mediapipe as mp
from HandDetectorModule import HandDetector

label = ['pose1', 'pose2']
def get_lmList(hand):
    c_lmlist = []
    # print(hand['lmList'])
    for count, lm in enumerate(hand['lmList']):
        # print(hand['lmList'])
        c_lmlist.append(lm[0])
        c_lmlist.append(lm[1])
        # c_lmlist.append(lm[2])
    # print(type(c_lmlist))
    return c_lmlist


cap = cv2.VideoCapture(0)
detector = HandDetector(detectionCon = 0.7, maxHands = 1)
no_time_step = 10
number_of_step = 1000
counts  = 0
lmList = []
my_list = []
while counts <= number_of_step:
    ret, frame = cap.read()
    hand, frame= detector.findHands(frame)
    cv2.imshow('cam', frame)
    if hand:
        # for step in range(0, number_of_step):
        for count in range(0, no_time_step):
            # lmList.append(get_lmList(hand[0]))
            lmArr = get_lmList(hand[0])
            lmList.append(lmArr)
        # print(len(lmList))
        # if len()
        print(counts)
        counts += 1
        my_list.append(lmList)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

df = pd.DataFrame(lmList)
df.to_csv("pose1.txt")
data = pd.read_csv("pose1.txt")
print(data.iloc[:0:].values)
cap.release()
cv2.destroyAllWindows()
