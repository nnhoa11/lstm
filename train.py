import os
import matplotlib.pyplot as plt
import matplotlib.image as mpmig
import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import pandas as pd
import random
from keras.preprocessing import image
import keras
from keras import layers
import cv2
from sklearn.model_selection import train_test_split
import mediapipe as mp
from keras.layers import LSTM, Dense,Dropout
from keras.models import Sequential

X = []
Y = []

data1 = pd.read_csv("pose1.txt")
data2 = pd.read_csv("pose2.txt")

dataset1 = data1.iloc[:,1:].values
dataset2 = data2.iloc[:,1:].values

no_timestep = 1

n_sample = 1000
for i in range(no_timestep, n_sample):
    X.append(dataset1[i-no_timestep:i,:])
    Y.append(1)
for i in range(no_timestep, n_sample):
    X.append(dataset2[i-no_timestep:i,:])
    Y.append(0)

X, Y = np.array(X), np.array(Y)
print(X.shape, Y.shape)

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)

model = Sequential()
model.add(LSTM(units = 50, return_sequences = True, input_shape = (X.shape[1], X.shape[2])))
model.add(Dropout(0.2))
model.add(LSTM(units = 50, return_sequences = True))
model.add(Dropout(0.2))
model.add(LSTM(units = 50, return_sequences = True))
model.add(Dropout(0.2))
model.add(LSTM(units = 50))
model.add(Dropout(0.2))
model.add(Dense(units = 1, activation="sigmoid"))
model.compile(optimizer="adam", metrics = ['accuracy'], loss = "binary_crossentropy")

model.fit(X_train, y_train, epochs=16,batch_size=32, validation_data=(X_test, y_test))
model.save("model.h5")
