import os
import threading

import matplotlib.pyplot as plt
import matplotlib.image as mpmig
import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import pandas as pd
import random
from keras.preprocessing import image
import keras
from keras import layers
import cv2
import mediapipe as mp
from HandDetectorModule import HandDetector

model = tf.keras.models.load_model("model.h5")
label = ['pose1', 'pose2']
def get_lmList(hand):
    c_lmlist = []
    # print(hand['lmList'])
    for count, lm in enumerate(hand['lmList']):
        # print(lm)
        c_lmlist.append(lm[0])
        c_lmlist.append(lm[1])

    # print(type(c_lmlist))
    return c_lmlist

def detect(model, lm_list):
    global label
    # print(len(lm_list))
    # lm_list = np.array(lm_list)
    lm_list_tensor = np.expand_dims(lm_list, axis=0)

    # print(lm_list_tensor.shape)
    # results = model.predict(lm_list_tensor)
    # if results > 0.5 : print("pose1")
    # else : print("pose2")
    # print(results)

cap = cv2.VideoCapture(0)
detector = HandDetector(detectionCon = 0.7, maxHands = 1)
no_time_step = 1
number_of_step = 300
counts  = 0
lmList = []
my_list = []
while True:
    ret, frame = cap.read()
    hand, frame= detector.findHands(frame)
    cv2.imshow('cam', frame)
    if counts > 6 * no_time_step:
        # print('start detecting...')
        if hand:
            my_list = get_lmList(hand[0])
            lmList.append(my_list)
            if (len(lmList) == no_time_step):
                t1 = threading.Thread(target=detect, args=(model, lmList))
                t1.start()
                lmList = []
    counts += 1
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
model.summary()
cap.release()
cv2.destroyAllWindows()
